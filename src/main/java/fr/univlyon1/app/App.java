package fr.univlyon1.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class App {

    public static void main(String[] args) {
        SpringApplication.run(App.class, args);
    }

    public static boolean isWorking() {
        // Method to indicate that the application is working correctly
        return true;
    }
}

package fr.univlyon1.app;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class AppTest {
    @Test
    public void testAppIsWorking() {
        App app = new App();
        assertTrue(app.isWorking(), "App should return true");
    }
}

